<?php
/**
 * Created by PhpStorm.
 * User: simorg
 * Date: 02/01/18
 * Time: 02:34 PM
 */

class Connection
{
    private $DBhost;
    private $DBuser;
    private $DBpass;
    private $DBname;
    private $connect;

    public function __construct(){
        $this->DBhost = "localhost";
        $this->DBuser = "root";
        $this->DBpass = "1234";
        $this->DBname = "cv";
    }

    public function connection(){
        try {
            $DBcon = new PDO("mysql:host=$this->DBhost;dbname=$this->DBname",$this->DBuser,$this->DBpass);
            $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $con = $this->connect = $DBcon;
            return $con;
        } catch(PDOException $ex){
            die($ex->getMessage());
        }
    }

    public function closeConnection(){
        $this->connect = null;
    }
}