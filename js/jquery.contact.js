document.addEventListener("DOMContentLoaded", function(){
	var button  = document.querySelector("#submit");
    var msjResp = document.querySelector("#simple-msg");


	button.addEventListener("click",function(){
        msjResp.innerHTML = "";
		var name    = document.getElementById("name").value;
		var email   = document.getElementById("email").value;
		var message = document.getElementById("comments").value;

		if(name.trim() === "" || email.trim() === "" || message.trim() === ""){
		    msjResp.innerHTML = "<strong>Ups... all fields are empty</strong>";
        }

        if(!validateEmail(email)){
		    msjResp.innerHTML = "<strong>The email is not valid</strong>";
        }

        var data = {
		    name:name,
            email:email,
            message:message,
            action:'savedata'
        };

        $.ajax({
            url:'/template-1/backend/controller/service.php',
            type: 'POST',
            dataType: 'json',
            data:data,
            success:function(response){
                //console.log(response);
                msjResp.innerHTML = "<strong>The Message Sent Successfully</strong>";
            },
            error:function(response){
                console.log(response);
            }
        });
        /*axios.post('/template-1/backend/controller/service.php',{
            data: data
        })
        .then(function (response) {
            console.log(response);

            if(response.status === 200 && response.data){
                msjResp.innerHTML = "<strong>Message Sent Successfully</strong>";
            }
        })
        .catch(function (error) {
            console.log(error);
        });*/
	});
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}